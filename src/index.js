import React from 'react';
import ReactDOM from 'react-dom';
import {Provider} from "react-redux";
import storeFactory from "./store/index";
import * as serviceWorker from './serviceWorker';

import {BrowserRouter} from "react-router-dom";
import {TruckApp, TruckEventListener} from "./components/containers";


const store = storeFactory();


window.React = React;
window.store = store;

ReactDOM.render(
    <BrowserRouter>
        <Provider store={store}>
            <TruckApp />
            <TruckEventListener />
        </Provider>
    </BrowserRouter>,
    document.getElementById('root')
);

serviceWorker.unregister();
