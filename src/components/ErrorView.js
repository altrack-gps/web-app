// import React from "react";
// import propTypes from 'prop-types'
// import {Alert} from "antd";
//
//
// export default class ErrorView extends React.Component {
//     static propTypes = {
//         onRemoveError: propTypes.func.isRequired
//     };
//
//     render() {
//         const {errors, onRemoveError} = this.props;
//         return (
//             <div>
//                 {errors.map((error, index) =>
//                     <Alert message="Error"
//                            description={error}
//                            type="error"
//                            key={index}
//                            closable
//                            onClose={() => onRemoveError(index)}
//                            showIcon />
//                 )}
//             </div>
//         );
//     }
// }