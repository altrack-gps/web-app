import React from "react";
import {Form, Icon, Input, Button, Col} from 'antd';


class LoginFormView extends React.Component {
    handleSubmit = (e) => {
        e.preventDefault();
        this.props.form.validateFields(async (err, values) => {
            if (!err)
                await this.props.onAuth(values.username, values.password);
        });
    };

    render() {
        const { getFieldDecorator } = this.props.form;
        return (
            <Col span={4} offset={10} className='mt-5 text-center'>
                <span className='altrack-card-header-text'>
                    ALTrack
                </span>

                <br/>

                <span className='altrack-truck-info'>
                    login form
                </span>

                <Form onSubmit={this.handleSubmit} className="login-form mt-3">
                    <Form.Item>
                        {getFieldDecorator('username', {
                            rules: [{
                                required: true,
                                message: 'Please input valid username!',
                                pattern: new RegExp("[A-Za-z0-9]+")
                            }],
                        })(
                            <Input prefix={<Icon type="user" style={{ color: 'rgba(0,0,0,.25)' }} />} placeholder="Username" />
                        )}
                    </Form.Item>
                    <Form.Item>
                        {getFieldDecorator('password', {
                            rules: [{
                                required: true,
                                message: 'Please input valid Password!',
                                pattern: new RegExp("[A-Za-z0-9]+")
                            }],
                        })(
                            <Input prefix={<Icon type="lock" style={{ color: 'rgba(0,0,0,.25)' }} />} type="password" placeholder="Password" />
                        )}
                    </Form.Item>
                    <Form.Item>
                        <Button type="primary" htmlType="submit" className="login-form-button">
                            Log in
                        </Button>
                    </Form.Item>
                </Form>
            </Col>
        );
    }
}

const LoginView = Form.create({ name: 'normal_login' })(LoginFormView);
export default LoginView;