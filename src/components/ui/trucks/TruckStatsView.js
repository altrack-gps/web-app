import React from "react";
import {Row, Col, Form} from "antd";

import Plot from 'react-plotly.js';
import YearPicker from "react-year-picker";

import * as api from "../../../altrackAPI/api";


export default class TruckStatsView extends React.Component {
    constructor(props) {
        super(props);

        this.truckID = this.props.match.params['id'];
        this.state = {
            stats: [],
            currentYear: new Date().getFullYear(),
            comparedYear: null,
            comparedStats: []
        };

        this.selectYear = this.selectYear.bind(this);
    }

    async componentDidMount() {
        await this.fetchStats();
    }

    async fetchStats() {
        const stats = await api.fetchTruckStats(this.truckID);
        await this.setState({stats});
    }

    async fetchComparedStats() {
        const comparedStats = await api.fetchTruckStats(
            this.truckID,
            {year:this.state.comparedYear}
        );
        await this.setState({comparedStats});
    }

    render () {
        const {stats, comparedStats, comparedYear, currentYear} = this.state;
        const s = [
            {
                x: Object.keys(stats),
                y: Object.values(stats),
                type: 'bar',
                name: currentYear
            },
            {
                x: Object.keys(comparedStats),
                y: Object.values(comparedStats),
                type: 'bar',
                name: comparedYear
            }
        ];

        return (
            <Row>
                <Col span={6}>
                    <div className="card border-bottom" style={{border: 'none'}}>
                        <div className="card-body p-0 m-2">
                            <h5 className="card-title altrack-card-header-text">Stats comparation</h5>

                            <div className="mt-3">
                                <Form.Item label='Select year' className="altrack-truck-info">
                                    <YearPicker className='altrack-truck-info ant-input'
                                                onChange={this.selectYear}/>
                                </Form.Item>
                            </div>

                        </div>
                    </div>
                </Col>

                <Col offset={6} className='altrack-truck-info'>
                    <Plot data={s} layout={this.layout} />
                </Col>
            </Row>
        );
    }

    async selectYear(year) {
        await this.setState({
            comparedYear: year
        });
        await this.fetchComparedStats();
    }

    get layout() {
        return {
            title: 'Stats by finished routes',
            yaxis: {
                dtick: 1,
                width: 4,
                title: 'Number of finished routes'
            },
            height: 500,
            width: 1200,
            bargap: 0.35,
            bargroupgap: 0.1
        };
    }
}