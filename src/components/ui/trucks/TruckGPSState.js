import React from "react";
import * as L from "leaflet";
import PropTypes from 'prop-types'
import {Marker} from "react-leaflet";


export default class TruckGPSStateView extends React.Component {
    static propTypes = {
        // truckID: PropTypes.number.isRequired,
        // route: PropTypes.object.isRequired
    };

    constructor(props) {
        super(props);

        this.ws = null;
        this.id = props.truckID;
        this.state = {
            lat: 49.377710,
            lon: 31.470635
        }
    }

    componentDidMount() {
        this.subscribeGPSState();
    }

    subscribeGPSState() {
        this.ws = new WebSocket(`ws://localhost:8080/truck-gps-state/${this.id}`);
        this.ws.onmessage = e => {
            const parsedState = JSON.parse(e.data);
            this.setState(parsedState);
            this.saveGpsState(parsedState)
        };
        this.ws.onerror = e => console.log(e, 'er');
    }

    saveGpsState(state) {
        this.props.saveGpsState(state);
    }

    componentWillUnmount() {
        this.unsubscribeGPSState();
    }

    unsubscribeGPSState() {
        this.ws.close();
        this.ws = null;
    }

    render() {
        const {lat, lon} = this.state;
        return (
            <Marker icon={TruckIcon} position={[lat, lon]} />
        );
    }
}

export const TruckIcon = new L.Icon({
    iconUrl: require('../../../img/truck.svg'),
    iconAnchor: null,
    popupAnchor: null,
    shadowUrl: null,
    shadowSize: null,
    shadowAnchor: null,
    iconSize: new L.Point(65, 75),
});
