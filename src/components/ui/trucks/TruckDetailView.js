import React from 'react'
import PropTypes from 'prop-types'
import {Col, Row} from "antd";
import {Link} from "react-router-dom";

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faChartBar, faRoad } from '@fortawesome/free-solid-svg-icons'


import TruckMapView from "./TruckMapView";
import {TruckEvent} from "../../containers";
import SelectEntityList from "../SelectEntityList";
import {formatUTCDateTime} from "../../../utils/time";
import * as api from "../../../altrackAPI/api";

import '../../../styles/style.css'
import {imeiMask, plateNumberMask} from "../../../utils/textMasks";




export default class TruckDetailView extends React.Component {
    static propTypes = {
        id: PropTypes.number.isRequired,
        truck: PropTypes.object.isRequired,
        route: PropTypes.object.isRequired,
        setRoute: PropTypes.func.isRequired,
        fetchTruck: PropTypes.func.isRequired,
        fetchRoute: PropTypes.func.isRequired,
    };

    constructor(props) {
        super(props);
        this.selectRoute = this.selectRoute.bind(this);
    }

    async componentDidMount() {
        await this.props.fetchTruck(this.props.id);
    }

    async selectRoute(routeID) {
        const {id} = this.props;
        await this.props.setRoute(id, routeID);
    }

    render() {
        const {truck, route, gpsState} = this.props;
        return (
            <div>
                <Row gutter={32}>
                    <Col span={6}>
                        <div className="card border-bottom" style={{border: 'none'}}>
                            <div className="card-body p-0 m-2">
                                <h5 className="card-title altrack-card-header-text">Truck info</h5>
                                <h6 className="card-subtitle mb-3 altrack-card-subtitle-link">
                                    <Link to={`/trucks/${truck.id}/history`}>View history</Link>
                                    <br/>
                                </h6>

                                <Row>
                                    <Col span={12} className='mt-3'>
                                        <span className="altrack-truck-info-header">IMEI</span>
                                        <br/>
                                        <span className="altrack-truck-info">
                                            {imeiMask(gpsState.imei)}
                                            </span>
                                    </Col>
                                    <Col span={12} className='mt-3'>
                                        <span className="altrack-truck-info-header">Plate number</span>
                                        <br/>
                                        <span className="altrack-truck-info">
                                            {plateNumberMask(truck.plateNumber)}
                                            </span>
                                    </Col>

                                    <Col span={12} className='mt-3'>
                                        <span className="altrack-truck-info-header">Lat</span>
                                        <br/>
                                        <span className="altrack-truck-info">{gpsState.lat}</span>
                                    </Col>
                                    <Col span={12} className='mt-3'>
                                        <span className="altrack-truck-info-header">Lon</span>
                                        <br/>
                                        <span className="altrack-truck-info">{gpsState.lon}</span>
                                    </Col>

                                    <Col span={12} className='mt-3'>
                                        <span className="altrack-truck-info-header">Course</span>
                                        <br/>
                                        <span className="altrack-truck-info">{gpsState.course}</span>
                                    </Col>
                                    <Col span={12} className='mt-3'>
                                        <span className="altrack-truck-info-header">Speed</span>
                                        <br/>
                                        <span className="altrack-truck-info">{gpsState.speed}</span>
                                    </Col>

                                    <Col span={12} className='mt-3'>
                                        <span className="altrack-truck-info-header">Last update</span>
                                        <br/>
                                        <span className="altrack-truck-info">
                                            {formatUTCDateTime(gpsState.state_at)}
                                        </span>
                                    </Col>
                                    <Col span={12}>

                                    </Col>

                                </Row>
                                <br/>
                                <span className='altrack-card-subtitle-link'>
                                    <span className='mr-1'>
                                        <FontAwesomeIcon icon={faChartBar}/>
                                    </span>
                                    <Link to={`/truck-stats/${truck.id}`}>truck stats</Link>
                                </span>
                            </div>
                        </div>

                        {(truck.id) ?
                            <TruckEvent truckID={truck.id} /> : ''
                        }

                        <div className="card border-bottom" style={{border: 'none'}}>
                            <div className="card-body p-0 m-2 mt-3">
                                <h5 className="card-title altrack-card-header-text">Route</h5>
                                <span className='mr-2'>
                                    <FontAwesomeIcon icon={faRoad}/>
                                </span>
                                <SelectEntityList
                                    width='26em'
                                    selectedEntity={truck.routeID}
                                    onChange={this.selectRoute}
                                    onFetch={api.fetchOnlyRouteTitles} />
                            </div>
                        </div>
                    </Col>
                    {(truck.id) ?
                        <TruckMapView truckID={truck.id} route={route}/> : ''}
                </Row>
            </div>
        );
    }
}