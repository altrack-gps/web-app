import React from "react";
import {Col, Row} from "antd";
import { formatUTCDateTime } from "../../../utils/time";
import {Link} from "react-router-dom";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faHistory, faLocationArrow, faMapMarkerAlt } from '@fortawesome/free-solid-svg-icons'


export default class TruckEventView extends React.Component {
    async componentDidMount() {
        await this.props.fetchLastEvent(this.props.truckID);
    }

    render() {
        const {event} = this.props;
        return (
            <div className="card border-bottom" style={{border: 'none'}}>
                <div className="card-body p-0 m-2 mt-3">
                    <h5 className="card-title altrack-card-header-text">Last truck event</h5>
                    <h6 className="card-subtitle mb-3 altrack-card-subtitle-link">
                        <Link to={`/truck-events/${this.props.truckID}`}>View history</Link>
                    </h6>

                    <Row>
                        <Col span={24}  className='mt-3'>
                            <Col span={2}>
                                <FontAwesomeIcon icon={faLocationArrow} />
                            </Col>
                            <Col span={22}>
                                <span className="altrack-truck-info-header">Action</span> <br/>
                                <span className="altrack-truck-info">{event.type}</span>
                            </Col>
                        </Col>

                        <Col span={24}  className='mt-3'>
                            <Col span={2}>
                                <FontAwesomeIcon icon={faHistory} />
                            </Col>
                            <Col span={22}>
                                <span className="altrack-truck-info-header">Time</span> <br/>
                                <span className="altrack-truck-info">{formatUTCDateTime(event.event_at)}</span>
                            </Col>
                        </Col>

                        {(event.payload) ?
                            <div>
                            {(event.payload.address) ?
                                <Col span={24}  className='mt-3'>
                                    <Col span={2}>
                                        <FontAwesomeIcon icon={faMapMarkerAlt} />
                                    </Col>
                                    <Col span={22}>
                                        <span className="altrack-truck-info-header">Address</span> <br/>
                                        <span className="altrack-truck-info">{event.payload.address}</span>
                                    </Col>
                                </Col> : ''
                            }
                        </div> : ''
                        }
                    </Row>
                </div>
            </div>
        );
    }
}