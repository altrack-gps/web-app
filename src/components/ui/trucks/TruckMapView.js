import React from "react";

import MapView from "../MapView";
import {RouteView} from "../routes/RouteView";
import {TruckGPSState} from "../../containers";


const TruckMapView = ({truckID, route}) =>
    <div>
        <MapView>
            <TruckGPSState truckID={truckID}/>
            <RouteView route={route} />
        </MapView>
    </div>;


TruckMapView.propTypes = {
    // truck_id: PropTypes.number.isRequired
};

export default TruckMapView;

