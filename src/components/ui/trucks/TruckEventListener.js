import React from "react";

import {formatLocalDateTime} from "../../../utils/time";
import {openNotification} from "../../../utils/messages";


export default class TruckEventListener extends React.Component{
    constructor(props) {
        super(props);
        this.ws = null;
    }

    componentDidMount() {
        this.subscribeGPSState();
    }

    subscribeGPSState() {
        this.ws = new WebSocket('ws://localhost:8080/truck-events');
        this.ws.onmessage = e => this.handleMessage(e);
        this.ws.onerror = e => console.log('error', e);
    }

    handleMessage(e) {
        const event = JSON.parse(e.data);
        const eventMsg = getEventMessage(event);

        const title = `Truck Event (${formatLocalDateTime(event.event_at)})`;
        openNotification(title, eventMsg);

        this.props.saveEvent(event);
    }

    componentWillUnmount() {
        this.ws.close();
    }

    render() {
        return null;
    }
}


const PASSING_WAY_POINT = 'PASSING_WAY_POINT';


const EventAction = {
    'LEFT_ROUTE': 'has left route',
    'START_ROUTE': 'started route',
    'FINISH_ROUTE': 'finished route',
    'MOVE_ON_ROUTE': 'has moved on route',
    'PASSING_WAY_POINT': 'is passing way point',
};


export const getEventAction = e => {
    let action = EventAction[e.type];
    console.log('ac', action);

    if (e.type === PASSING_WAY_POINT)
        action += ` (${e.payload.address})`;

    return action;
};


const getEventMessage = e => {
    let action = getEventAction(e);
    return `Truck (${e.plate_number}, ${e.imei}) ${action}`;
};
