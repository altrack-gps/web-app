import React from 'react'
import PropTypes from 'prop-types'
import {Button, Form, Input} from "antd";


const CreateTruckForm = ({history, onCreateTruck}) => {
    let submit = async e => {
        e.preventDefault();
        const imei = e.target.elements.imei.value;
        const plateNumber = e.target.elements.plateNumber.value;

        if (imei.length && plateNumber.length) {
            await onCreateTruck(imei, plateNumber);
            history.push('/trucks');
        }
    };

    return (
        <Form layout='horizontal' onSubmit={submit}>
            <Form.Item>
                <Input name='imei'
                       type='number'
                       placeholder='Imei'/>
            </Form.Item>

            <Form.Item>
                <Input name='plateNumber'
                       type='text'
                       placeholder='Plate number'/>
            </Form.Item>

            <Form.Item>
                <Button type="primary"
                        htmlType="submit">
                    Create
                </Button>
            </Form.Item>
        </Form>
    );
};

CreateTruckForm.propTypes = {
    onCreateTruck: PropTypes.func.isRequired
};

export default CreateTruckForm;