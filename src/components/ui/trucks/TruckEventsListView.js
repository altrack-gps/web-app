import React from "react";
import {Button, Col, DatePicker, Row, Table} from "antd";

import * as api from "../../../altrackAPI/api";
import {formatLocalDateTime, formatUTCDateTime} from "../../../utils/time";


const { RangePicker } = DatePicker;


export default class TruckEventListView extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            events: [],
            isLoading: false
        };
        this.truckID = this.props.truckID;

        this.reset = this.reset.bind(this);
        this.submit = this.submit.bind(this);
        this.fetchEvents = this.fetchEvents.bind(this);
    }

    async componentDidMount() {
        await this.fetchEvents();
    }

    async fetchEvents(params={}) {
        try {
            await this.setState({
                isLoading: true
            });
            const events = await api.fetchEvents(this.truckID, params);
            await this.setState({
                events: events,
                isLoading: false
            });
        } catch (e) {
            console.log(e);
        }
    }

    async submit(values) {
        const params = {
            'event_at_after': formatUTCDateTime(values[0]),
            'event_at_before': formatUTCDateTime(values[1])
        };
        await this.fetchEvents(params);
    }

    async reset() {
        await this.fetchEvents();
    }

    render() {
        const {isLoading} = this.state;

        return (
            <Row className='altrack-truck-info'>
                <Col span={10} offset={9}>
                    <RangePicker
                        showTime={{format: 'HH:mm:ss'}}
                        format="YYYY-MM-DD HH:mm:ss"
                        style={{width: '26em'}}
                        placeholder={['Start Time', 'End Time']}
                        onOk={this.submit}/>
                    <Button type='primary' onClick={this.reset}>Reset</Button>
                </Col>

                <br/>
                <br/>

                <Col span={14} offset={5}>
                    <Table dataSource={this.rows}
                           columns={this.columns}
                           scroll={{ y: 550}}
                           className='table'
                           loading={isLoading}
                           useFixedHeader />
                </Col>
            </Row>
        );
    }

    get columns() {
        return [
            {
                title: '#',
                dataIndex: 'key',
                width: '5em'
            },
            {
                title: 'Type',
                dataIndex: 'type',
                width: '15em',
            },
            {
                title: 'Date',
                dataIndex: 'event_at',
                width: '15em',
                render: dt => formatLocalDateTime(dt)
            },
            {
                title: 'Info',
                dataIndex: 'payload',
                width: '25em',
                render: payload => (payload) ? payload.address : ''
            },
        ]
    };

    get rows() {
        let rows = [];
        const {events} = this.state;

        events.forEach((e, i) => rows.push({
            key: i + 1,
            type: e.type,
            event_at: e.event_at,
            payload: e.payload
        }));
        return rows;
    }
}