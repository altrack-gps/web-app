import * as React from "react";
import {Col, DatePicker, Row} from "antd";

import MapView from "../MapView";
import {showWarning} from "../../../utils/messages";
import {RouteLineView} from "../routes/RouteView";
import {formatUTCDateTime} from "../../../utils/time";
import {computeDuration} from "../../../utils/osrmAPI";
import {fetchTruckHistory} from "../../../altrackAPI/api";


const { RangePicker } = DatePicker;


export default class TruckHistoryView extends React.Component {
    constructor(props) {
        super(props);

        this.truckID = props.match.params['id'];
        this.submitForm = this.submitForm.bind(this);

        this.state = {
            line: [],
            distance: 0,
            duration: 0
        };
    }


    async submitForm(data) {
        const startDate = formatUTCDateTime(data[0]);
        const stopDate = formatUTCDateTime(data[1]);

        try {
            const params = {
                'state_at_after': startDate,
                'state_at_before': stopDate
            };
            const response = await fetchTruckHistory(this.truckID, params);
            const points = response.map(
                state => [state.position.lat, state.position.lng]
            );
            if (points.length) {
                const route = await computeDuration(points);
                await this.setState({
                    line: points,
                    duration: route.duration,
                    distance: route.distance
                });
            }
            else {
                showWarning('There are no data to show');
            }
        } catch (e) {
            console.error(e);
        }
    }

    render() {
        const {line, duration, distance} = this.state;

        return (
            <div>
                <Row gutter={32}>
                    <Col span={6}>
                        <div className="card border-bottom" style={{border: 'none'}}>
                            <div className="card-body p-0 m-2">
                                <h5 className="card-title altrack-card-header-text">Truck history</h5>

                                <div className='mt-3'>
                                    <span className="altrack-truck-info-header">Date range</span>
                                    <RangePicker
                                        showTime={{format: 'HH:mm:ss'}}
                                        format="YYYY-MM-DD HH:mm:ss"
                                        style={{width: '26em'}}
                                        className='altrack-truck-info'
                                        placeholder={['Start Time', 'End Time']}
                                        onOk={this.submitForm}/>
                                </div>

                                <br/>

                                <Row className='mt-3'>
                                    <Col span={12}>
                                        <span className="altrack-truck-info-header">Distance</span>
                                        <br/>
                                        <span className="altrack-truck-info">{distance}</span>
                                    </Col>

                                    <Col span={12}>
                                        <span className="altrack-truck-info-header">Duration</span>
                                        <br/>
                                        <span className="altrack-truck-info">{duration}</span>
                                    </Col>
                                </Row>

                            </div>
                        </div>

                    </Col>
                    <MapView>
                        <RouteLineView positions={line} />
                    </MapView>
                </Row>
            </div>
        );
    }
}