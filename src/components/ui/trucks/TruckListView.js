import React from 'react'
import {Button, Col, Form, Input, Row, Table} from 'antd';
import {Link} from "react-router-dom";
import PropTypes from 'prop-types'
import SelectEntityList from "../SelectEntityList";
import * as api from "../../../altrackAPI/api";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faSearch} from "@fortawesome/free-solid-svg-icons";
import {imeiMask, plateNumberMask} from "../../../utils/textMasks";


export default class TruckListView extends React.Component {
    static propTypes = {
        trucks: PropTypes.array.isRequired,
        onFetchTrucks: PropTypes.func.isRequired
    };

    constructor(props) {
        super(props);

        this.searchTruck = this.searchTruck.bind(this);
    }

    async componentDidMount() {
        await this.fetchTrucks();
    }

    async fetchTrucks(params={}) {
        await this.props.onFetchTrucks(params);
    }

    async searchTruck(params) {
        await this.fetchTrucks(params);
    }

    get columns() {
        return [
            {
                title: '#',
                dataIndex: 'key',
                width: '5em'
            },
            {
                title: 'Plate number',
                dataIndex: 'plateNumber',
                width: '15em',
                render: text => plateNumberMask(text)
            },
            {
                title: 'Imei',
                dataIndex: 'imei',
                width: '25em',
                render: text => imeiMask(text)
            },
            {
                title: 'Route',
                dataIndex: 'routeTitle',
                width: '15em',
                render: (text, truck) => <Link to={`/routes/${truck.routeID}/edit`}>{text}</Link>
            },
            {
                title: '',
                width: '10em',
                render: (text, record) => {
                    const onView = () => this.props.history.push(`/trucks/${record.id}`);

                    return (
                        <span onClick={onView}>
                            <Button htmlType='button'
                                    type='primary'
                                    icon='eye'
                                    onClick={onView}/>
                        </span>
                    )
                },
            }
        ]
    };

    get rows() {
        let rows = [];
        const {trucks} = this.props;

        trucks.forEach((t, i) => rows.push({
            key: i + 1,
            onRemove: this.removeTruck,
            ...t
        }));
        return rows;
    }

    render() {
        return (
            <div>
                <Row>
                    <Col span={6}>
                        <div className="card border-bottom" style={{border: "none"}}>
                            <div className="card-body p-0 m-2">
                                <h5 className="card-title altrack-card-header-text">Search</h5>
                                <TruckSearchForm onSearch={this.searchTruck}/>
                            </div>
                        </div>

                    </Col>

                    <Col span={16} offset={2}>
                        <Table dataSource={this.rows}
                               columns={this.columns}
                               className='table thead-dark altrack-truck-info'
                               scroll={{ y: 550}}
                               useFixedHeader />
                    </Col>
                </Row>
            </div>
        );
    }
}


class TruckSearchFormView extends React.Component {
    constructor(props) {
        super(props);

        this.handleSubmit = this.handleSubmit.bind(this);
        this.onRouteChange = this.onRouteChange.bind(this);
    }

    handleSubmit(e) {
        e.preventDefault();

        this.props.form.validateFields((err, values) => {
            if (!err) {
                this.props.onSearch(values);
                this.props.form.resetFields();
            }
        });
    }

    onRouteChange(routeID) {
    }

    render() {
        const {getFieldDecorator} = this.props.form;

        return (
            <Form onSubmit={this.handleSubmit} className="form">
                <Form.Item label='Imei' className="altrack-truck-info">
                    {getFieldDecorator('imei', {
                        rules: [{
                            required: false,
                            message: 'Please input truck imei!',
                            pattern: new RegExp('[0-9]+')
                        }],
                    })(
                        <Input placeholder="04343434334"/>
                    )}
                </Form.Item>
                <Form.Item label='Plate number' className="altrack-truck-info">
                    {getFieldDecorator('plate_number', {
                        rules: [{
                            required: false,
                            message: 'Please input plate number!',
                            pattern: new RegExp("[A-ZА-Я]{2}[0-9]{4}[A-ZА-Я]{2}")
                        }],
                    })(
                        <Input placeholder="AA1111AA"/>
                    )}
                </Form.Item>
                <Form.Item label='Route' className="altrack-truck-info">
                    {getFieldDecorator('route', {})(
                        <SelectEntityList onChange={f => f}
                                          onFetch={api.fetchOnlyRouteTitles} />
                    )}
                </Form.Item>
                <Form.Item>
                    <Button type="primary" htmlType="submit">
                        search <FontAwesomeIcon icon={faSearch} className='ml-1'/>
                    </Button>
                </Form.Item>
            </Form>
        );
    }
}


const TruckSearchForm = Form.create({name: 'searchTruck'})(TruckSearchFormView);