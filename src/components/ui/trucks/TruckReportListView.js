import React from "react";
import * as api from "../../../altrackAPI/api";
import {Button, Col, DatePicker, Form, Row, Table} from "antd";
import {formatDate, formatUTCDateTime} from "../../../utils/time";
import {Link} from "react-router-dom";
import SelectEntityList from "../SelectEntityList";

import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faCheck, faBan, faSearch} from "@fortawesome/free-solid-svg-icons";
import {plateNumberMask} from "../../../utils/textMasks";


const { RangePicker } = DatePicker;


export default class TruckReportListView extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            reports: [],
            isLoading: false
        };

        this.fetchReports = this.fetchReports.bind(this);
    }

    async componentDidMount() {
        await this.fetchReports();
    }

    async fetchReports(params={}) {
        try {
            await this.setState({
                isLoading: true
            });
            const reports = await api.fetchReports(params);

            await this.setState({
                reports: reports,
                isLoading: false
            });
        } catch (e) {
            console.log('Caught error', e);
        }
    }

    render() {
        const {isLoading} = this.state;
        return (
            <Row>
                <Col span={6}>
                    <div className="card border-bottom" style={{border: 'none'}}>
                        <div className="card-body p-0 m-2 mt-3">
                            <h5 className="card-title altrack-card-header-text">Search</h5>
                            <TruckReportSearchForm onSearch={this.fetchReports} />
                        </div>
                    </div>
                </Col>

                <Col span={16} offset={2}>
                    <Table dataSource={this.rows}
                           columns={this.columns}
                           className='altrack-truck-info table'
                           scroll={{ y: 550}}
                           loading={isLoading}
                           useFixedHeader />
                </Col>
            </Row>
        );
    }

    get columns() {
        return [
            {
                title: '#',
                dataIndex: 'key',
                width: '5em'
            },
            {
                title: 'Truck',
                dataIndex: 'truckTitle',
                width: '15em',
                render: (text, report) => <Link to={`/trucks/${report.truck}`}>{plateNumberMask(text)}</Link>
            },
            {
                title: 'Route',
                dataIndex: 'routeTitle',
                width: '15em',
                render: (text, report) => <Link to={`/routes/${report.route}`}>{text}</Link>
            },
            {
                title: 'Starting date',
                dataIndex: 'start_date',
                width: '15em',
                render: date => formatUTCDateTime(date)
            },
            {
                title: 'Finishing date',
                dataIndex: 'finish_date',
                width: '15em',
                render: date => (date) ? formatUTCDateTime(date) : '-----------------------'
            },
            {
                key: 'isFinished',
                title: 'Finished',
                dataIndex: 'finish_date',
                width: '8em',
                render: date => <FontAwesomeIcon icon={(date) ? faCheck : faBan}/>
            }
        ]
    }

    get rows() {
        let rows = [];
        const {reports} = this.state;

        reports.forEach((t, i) => rows.push({
            key: i + 1,
            onRemove: this.removeTruck,
            ...t
        }));
        return rows;
    }
}


class TruckReportSearchFormView extends React.Component {
    constructor(props) {
        super(props);

        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleSubmit(e) {
        e.preventDefault();

        this.props.form.validateFields((err, values) => {
            if (!err) {
                if (values['start_date'] || values['finish_date'])
                    values = this.prepareParams(values);

                this.props.onSearch(values);
            }
        });
    }

    prepareParams(formParams) {
        let params = {...formParams};

        for (let key of Object.keys(params)) {
            if (key.match('_date') !== null) {
                if (params[key]) {
                    const convertedParams = this.convertDateParam(key, params[key]);
                    delete params[key];

                    params = {
                        ...params,
                        ...convertedParams
                    }
                }
            }
        }
        return params;
    }

    convertDateParam(name, values) {
        const params = {};
        params[`${name}_after`] = formatDate(values[0]);
        params[`${name}_before`] = formatDate(values[1]);

        return params;
    }

    render() {
        const {getFieldDecorator} = this.props.form;

        return (
            <Form onSubmit={this.handleSubmit} className="form altrack-truck-info">
                <Form.Item label='Truck'>
                    {getFieldDecorator('truck', {})(
                        <SelectEntityList onChange={f => f}
                                          onFetch={api.fetchTruckTitles} />
                    )}
                </Form.Item>
                <Form.Item label='Route'>
                    {getFieldDecorator('route', {})(
                        <SelectEntityList onChange={f => f}
                                          onFetch={api.fetchOnlyRouteTitles} />
                    )}
                </Form.Item>
                <Form.Item label='Start date'>
                    {getFieldDecorator('start_date', {})(
                        <RangePicker
                            format="YYYY-MM-DD"
                            placeholder={['Start Time', 'End Time']}/>
                    )}
                </Form.Item>
                <Form.Item label='Finish date'>
                    {getFieldDecorator('finish_date', {})(
                        <RangePicker
                            format="YYYY-MM-DD"
                            placeholder={['Start Time', 'End Time']}/>
                    )}
                </Form.Item>
                <Form.Item>
                    <Button type="primary" htmlType="submit">
                        <span className='mr-1'>search</span>
                        <FontAwesomeIcon icon={faSearch}/>
                    </Button>
                </Form.Item>
            </Form>
        );
    }
}


const TruckReportSearchForm = Form.create({name: 'searchTruck'})(TruckReportSearchFormView);