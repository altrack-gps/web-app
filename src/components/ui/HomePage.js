import React from "react";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faThumbsUp} from "@fortawesome/free-regular-svg-icons";
import {Link} from "react-router-dom";


const Home = () =>
    <div className='col-md-6 offset-3 text-center'>
        <span className='altrack-card-header-text'>
            ALTrack
        </span>
        <br/>
        <span className="altrack-card-subtitle-link">
            Real-time gps monitoring system
        </span>

        <br/>
        <br/>
        <br/>

        <span className='altrack-truck-info-header'>
            This app provides next features
        </span>

        <div className='mt-3'>
            <FeatureItem text='Monitor truck moving in real-time mode'
                         link={'/trucks/'}/>

            <FeatureItem text='Building routes with control points'
                         link={'/routes/'}/>

            <FeatureItem text='Viewing truck events in real-time mode' />
            <FeatureItem text='Viewing reports about finished routes'
                         link={'/truck-reports/'}/>

            <FeatureItem text='Viewing truck statistics of finished routes' />
            <FeatureItem text='Viewing history of truck moving' />
            <FeatureItem text='Viewing history of truck events' />
        </div>

    </div>;
export default Home;


const FeatureItem = ({text, link=''}) =>
    <div className="alert alert-info col-md-6 offset-3 altrack-truck-info" role="alert">
        <FontAwesomeIcon icon={faThumbsUp} />
        <span className="ml-2">
            {(link) ?
                <Link className='alert-link' to={link}>{text}</Link> :
                text
            }
        </span>
    </div>;