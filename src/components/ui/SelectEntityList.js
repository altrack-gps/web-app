import React from "react";
import {Select} from "antd";
import PropTypes from 'prop-types'


export default class SelectEntityList extends React.Component {
    static propTypes = {
        selectedEntity: PropTypes.number,
        onFetch: PropTypes.func.isRequired,
        onChange: PropTypes.func.isRequired,
        width: PropTypes.string
    };

    constructor(props) {
        super(props);

        this.state = this.getInitialState();
        this.selectEntity = this.selectEntity.bind(this);
        this.selectParams = this.selectParams.bind(this);
        this.getSecondKey = this.getSecondKey.bind(this);
    }

    getInitialState() {
        return {
            choices: []
        }
    }

    async componentDidMount() {
        const choices = await this.props.onFetch();
        await this.setState({
            choices: choices
        });
    }

    async selectEntity(value) {
        await this.props.onChange(value);
    }

    render() {
        const {choices} = this.state;
        return (
            <Select {...this.selectParams()}>

                <Select.Option value={null}> ---- </Select.Option>

                {choices.map((r, i) =>
                    <Select.Option value={r.id}
                                   key={i}>
                        {r[this.getSecondKey(r)]}
                    </Select.Option>
                )}
            </Select>
        );
    }

    selectParams() {
        let params = {
            onChange: this.selectEntity,
        };

        if (this.props.width)
            params['style'] = {width: this.props.width};

        if (this.props.selectedEntity)
            params['value'] = this.props.selectedEntity;

        return params;
    }

    getSecondKey(item) {
        return Object.keys(item).filter(key => key !== 'id')[0];
    }
}