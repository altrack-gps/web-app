import React from "react";
import {Button, Form, Input, Select} from "antd";


const { Option } = Select;

class ControlPointEditInnerForm extends React.Component {
    constructor(props) {
        super(props);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleSubmit(e) {
        e.preventDefault();
        this.props.form.validateFields((err, values) => {
            if (!err) {
                const {number} = this.props.point;
                this.props.onEdit(number, values);
            }
        });
    }

    render() {
        const {getFieldDecorator} = this.props.form;
        const {point} = this.props;
        return (
            <Form onSubmit={this.handleSubmit} className="login-form">
                <Form.Item>
                    {getFieldDecorator('houseNumber', {
                        initialValue: point.houseNumber,
                        rules: [{
                            required: true,
                            message: 'Please input house number!',
                            pattern: new RegExp("[0-9]+([A-Za-zА-Яа-я]{1})?")
                        }],
                    })(
                        <Input placeholder="House number"/>
                    )}
                </Form.Item>
                <Form.Item>
                    {getFieldDecorator('road', {
                        initialValue: point.road,
                        rules: [{required: true, message: 'Please input name of the road'}],
                    })(
                        <Input placeholder="Road"
                               disabled={point.road !== null}/>
                    )}
                </Form.Item>
                <Form.Item>
                    {getFieldDecorator('locality', {
                        initialValue: point.locality,
                        rules: [{required: true, message: 'Please input name of the locality'}],
                    })(
                        <Input placeholder="Locality (city or village)"
                               disabled={point.locality !== null}/>
                    )}
                </Form.Item>
                <Form.Item>
                    {getFieldDecorator('localityType', {
                        initialValue: point.localityType
                    })(
                        <Select disabled={point.localityType !== null}>

                            <Option value="CITY">City</Option>
                            <Option value="VILLAGE">Village</Option>
                        </Select>
                    )}
                </Form.Item>
                <Form.Item>
                    <Button.Group>
                        <Button type="dashed"
                                htmlType="button"
                                className="login-form-button"
                                onClick={this.props.onCancel}>
                            Cancel
                        </Button>
                        <Button type="primary"
                                htmlType="submit"
                                className="login-form-button">
                            Save
                        </Button>
                    </Button.Group>
                </Form.Item>
            </Form>
        );
    }
}

export const ControlPointEditForm = Form.create({name: 'normal_login'})(ControlPointEditInnerForm);