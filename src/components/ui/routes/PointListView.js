import React from "react";
import {Button, Checkbox, List, Modal} from "antd";
import {ControlPointEditForm} from "./ControlPointEditForm";
import WayPointService from "../../../services/WayPointService";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faMapMarkerAlt} from "@fortawesome/free-solid-svg-icons";
import {faEdit, faTrashAlt} from "@fortawesome/free-regular-svg-icons";
import {fromWayPointToAddress} from "../../../utils/wayPoints";


export class PointListView extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            isEditing: false,
            editingPoint: null
        };

        this.edit = this.edit.bind(this);
        this.onEdit = this.onEdit.bind(this);
        this.handleOk = this.handleOk.bind(this);
        this.canBeEdited = this.canBeEdited.bind(this);
        this.renderPoint = this.renderPoint.bind(this);
        this.cancel = this.cancel.bind(this);
    }

    render() {
        const {points} = this.props;
        const {isEditing} = this.state;

        return (
            <div>
                <List size="small"
                      className='altrack-truck-info'
                      dataSource={points}
                      renderItem={this.renderPoint}/>

                <Modal title="Basic Modal"
                       visible={isEditing}
                       footer={[
                            null,
                            null
                       ]}>

                    {this.renderForm()}
                </Modal>
            </div>
        );
    }

    renderPoint = point => {
        const {removeWayPoint, updateWayPoint, points} = this.props;

        const removePoint = () => removeWayPoint(point.number);
        const editPoint = () => this.edit(point);

        const changePointType = async (e, number) => {
            const point = WayPointService.findByNumber(
                points,
                number
            );
            await updateWayPoint({
                ...point,
                isControlPoint: e.target.checked
            });
        };

        return (
            <List.Item actions={[
                <Checkbox onChange={(e) => changePointType(e, point.number)}
                          checked={point.isControlPoint}
                          disabled={!this.canBeEdited(point.number)}/>,

                <span onClick={editPoint}
                      disabled={!point.isControlPoint}>
                    <FontAwesomeIcon icon={faEdit}/>
                </span>,

                <span onClick={removePoint}>
                    <FontAwesomeIcon icon={faTrashAlt} style={{color: 'red'}}/>
                </span>
            ]}>
                <span className='altrack-card-header-text'>{point.number + 1}</span>
                <span className='ml-3'>
                      <FontAwesomeIcon icon={faMapMarkerAlt}
                                       style={{color: '#ffa500'}}/>
                      <span className="ml-2">
                          {fromWayPointToAddress(point)}
                      </span>
                  </span>
            </List.Item>
        );
    };

    renderForm() {
        const {isEditing, editingPoint} = this.state;

        if (isEditing) {
            return <ControlPointEditForm
                        point={editingPoint}
                        onEdit={this.onEdit}
                        onCancel={this.cancel}/>
        }
    }

    async onEdit(number, address) {
        const point = WayPointService.findByNumber(
            this.props.points,
            number
        );
        await this.props.updateWayPointAddress({
            ...point,
            ...address
        });

        this.handleOk();
    }

    handleOk() {
        this.setState({
            isEditing: false,
            editingPoint: null
        })
    }

    cancel() {
        this.setState({
            isEditing: false,
            editingPoint: null
        })
    }

    edit(point) {
        if (point.isControlPoint) {
            this.setState({
                isEditing: true,
                editingPoint: point
            });
        }
    }

    canBeEdited(number) {
        const {points} = this.props;
        return (number !== 0 && number !== points.length - 1);
    }
}