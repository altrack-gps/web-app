import RouteBuilder from "./RouteBuilder";


export default class CreateRouteView extends RouteBuilder {
    resetRoute() {
        this.props.resetRoute();
    }

    async onSave(title) {
        const {route} = this.props;
        await this.props.saveRoute({
            ...route,
            title: title,
        });
    }
}