import RouteBuilder from "./RouteBuilder";


export default class EditRouteView extends RouteBuilder {
    async componentDidMount() {
        await this.props.fetchRoute(this.props.id);
        super.componentDidMount();
    }

    resetRoute() {
        window.location.reload();
    }

    onSave(title) {
        const {id, route} = this.props;
        this.props.saveRoute(id, {
            ...route,
            title: title
        });
    }
}