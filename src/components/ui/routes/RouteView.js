import React from "react";
import {Marker, Polyline} from "react-leaflet";

import {getPointIcon} from "../../../utils/routes";


export const RouteView = ({route}) => {
    const wayPoints = () => (route) ? route.wayPoints : [];
    const positions = () => (route) ? route.line: [];

    return (
        <div>
            <RouteLineView positions={positions()}/>
            <WayPointsView points={wayPoints()}/>
        </div>
    );
};

export const RouteLineView = ({positions}) => <Polyline positions={positions} />;

const WayPointsView = ({points}) => (
    <div>
        {points.map((p, i) =>
            <Marker position={[p.lat, p.lng]}
                          icon={getPointIcon(i, points)}
                          key={i}>
            </Marker>
        )}
    </div>
);