import React from "react";
import {Card, Col, Row, Tag} from "antd";
import {Marker, Tooltip} from "react-leaflet";

import MapView from "../MapView";
import {RouteLineView} from "./RouteView";
import {PointList} from "../../containers";
import {showError} from "../../../utils/messages";
import RouteControlForm from "./RouteControlForm";
import WayPointService from "../../../services/WayPointService";
import {allAddressesFilled, getPointIcon} from "../../../utils/routes";
import {Link} from "react-router-dom";
import {route} from "../../../store/reducers/routes";


export default class RouteBuilder extends React.Component {
    constructor(props) {
        super(props);

        this.addPoint = this.addPoint.bind(this);
        this.saveRoute = this.saveRoute.bind(this);
        this.resetRoute = this.resetRoute.bind(this);
        this.onDragPoint = this.onDragPoint.bind(this);
    }

    componentDidMount() {
        WayPointService.showNotification(
            this.props.route.wayPoints.length
        );
    }

    componentWillUnmount() {
        this.props.resetRoute();
    }

    render() {
        const { wayPoints, line, title } = this.props.route;
        return (
            <div>
                <Row gutter={32}>
                    <Col span={6}>
                        <RouteInfo {...this.props.route}/>
                        <br/>

                        <div className="card border-bottom" style={{border: 'none'}}>
                            <div className="card-body p-0 m-2 mt-3">
                                <h5 className="card-title altrack-card-header-text">Way points</h5>
                                <PointList points={wayPoints} /><br/>
                            </div>
                        </div>

                        <div className="card border-bottom" style={{border: 'none'}}>
                            <div className="card-body p-0 m-2 mt-3">
                                <h5 className="card-title altrack-card-header-text">Change title</h5>

                                <RouteControlForm
                                    title={title}
                                    onResetRoute={this.resetRoute}
                                    onSaveRoute={this.saveRoute} />
                            </div>
                        </div>
                    </Col>

                    <MapView onClick={this.addPoint}>
                        {wayPoints.map(p =>
                            <Marker position={[p.lat, p.lng]}
                                    onDragEnd={(e) => this.onDragPoint(e, p.number)}
                                    icon={getPointIcon(p, wayPoints)}
                                    key={p.number}
                                    draggable>
                                <Tooltip permanent direction="top">{String(p.number + 1)}</Tooltip>
                            </Marker>
                        )}

                        <RouteLineView positions={line}/>
                    </MapView>
                </Row>
            </div>
        );
    }

    resetRoute() {

    }

    async saveRoute(title) {
        const {wayPoints} = this.props.route;
        const wayPointsLength = wayPoints.length;

        try {
            if (wayPointsLength < 2) {
                showError('You have to add at least 2 points');
                return;
            }
            await this.onSave(title);
            this.props.history.push('/routes/');
        } catch (e) {
            showError('Route has not been saved ' + String(e));  // TODO parse response and show appropriate message
        }
    }

    async onSave(title) {

    }

    async addPoint(e) {
        await this.props.addWayPoint(e.latlng);
    }

    async onDragPoint(e, number) {
        const wayPoint = WayPointService.findByNumber(
            this.props.route.wayPoints,
            number
        );
        await this.props.updateWayPoint({
            ...wayPoint,
            ...e.target._latlng
        });
    }
}


export const RouteInfo = ({distance, duration, title=''}) => (
    <div className="card border-bottom" style={{border: 'none'}}>
        <div className="card-body p-0 m-2 mt-3">
            <h5 className="card-title altrack-card-header-text">Route details</h5>
            <Row>
                <Col span={12} className='mt-3'>
                    <span className="altrack-truck-info-header">Duration</span>
                    <br/>
                    <span className="altrack-truck-info">{duration}</span>
                </Col>

                <Col span={12} className='mt-3'>
                    <span className="altrack-truck-info-header">Distance</span>
                    <br/>
                    <span className="altrack-truck-info">{distance}</span>
                </Col>

                <Col span={12} className='mt-3'>
                    <span className="altrack-truck-info-header">Title</span>
                    <br/>
                    <span className="altrack-truck-info">{title}</span>
                </Col>
            </Row>

        </div>
    </div>
);