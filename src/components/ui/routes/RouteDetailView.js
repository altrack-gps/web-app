import React from "react";
import {Button, Checkbox, Col, List, Row, Tag} from "antd";
import {RouteView} from "./RouteView";
import MapView from "../MapView";
import {RouteInfo} from "./RouteBuilder";
import {PointList} from "../../containers";
import {Link} from "react-router-dom";
import {fromWayPointToAddress} from "../../../utils/wayPoints";

import {faMapMarkerAlt} from "@fortawesome/free-solid-svg-icons";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";


export default class RouteDetailView extends React.Component {
    async componentDidMount() {
        await this.props.fetchRoute(this.props.id);
    }

    render() {
        const {route} = this.props;
        return (
            <div>
                <Row gutter={32}>
                    <Col span={6}>
                        <div className="card border-bottom" style={{border: 'none'}}>
                            <div className="card-body p-0 m-2 mt-3">
                                <h5 className="card-title altrack-card-header-text">Route details</h5>
                                <h6 className="card-subtitle mb-3 altrack-card-subtitle-link">
                                    <Link to={`/routes/${route.id}/edit`}>Edit</Link>
                                </h6>
                                <Row>
                                    <Col span={12} className='mt-3'>
                                        <span className="altrack-truck-info-header">Duration</span>
                                        <br/>
                                        <span className="altrack-truck-info">{route.duration}</span>
                                    </Col>

                                    <Col span={12} className='mt-3'>
                                        <span className="altrack-truck-info-header">Distance</span>
                                        <br/>
                                        <span className="altrack-truck-info">{route.distance}</span>
                                    </Col>

                                    <Col span={12} className='mt-3'>
                                        <span className="altrack-truck-info-header">Title</span>
                                        <br/>
                                        <span className="altrack-truck-info">{route.title}</span>
                                    </Col>
                                </Row>

                            </div>
                        </div>
                        <br/>

                        <div className="card border-bottom" style={{border: 'none'}}>
                            <div className="card-body p-0 m-2 mt-3">
                                <h5 className="card-title altrack-card-header-text">Way points</h5>

                                <List size="small"
                                      className='altrack-truck-info'
                                      dataSource={route.wayPoints}
                                      renderItem={point =>
                                          <List.Item>
                                              <span className='altrack-card-header-text'>{point.number + 1}</span>
                                              <span className='ml-3'>
                                                  <FontAwesomeIcon icon={faMapMarkerAlt}
                                                         style={{color: '#ffa500'}}/>
                                                  <span className="ml-2">
                                                      {fromWayPointToAddress(point)}
                                                  </span>
                                              </span>
                                          </List.Item>}/>
                            </div>
                        </div>
                    </Col>

                    <MapView>
                        <RouteView route={route}/>
                    </MapView>
                </Row>
            </div>
        );
    }
}