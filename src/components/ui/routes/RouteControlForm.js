import React from "react";
import {Button, Form, Input} from "antd";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faBan, faSave} from "@fortawesome/free-solid-svg-icons";


const ControlForm = props => {
    const { getFieldDecorator } = props.form;
    const { form, onResetRoute, onSaveRoute, title } = props;

    const submitForm = e => {
        e.preventDefault();
        props.form.validateFields(async (err, values) => {
            if (!err) {
                await onSaveRoute(values.title);
            }
        });
    };

    const resetForm = e => {
        e.preventDefault();
        form.setFieldsValue({title: title});

        onResetRoute();
    };

    return (
        <Form layout='inline'
              onSubmit={submitForm}
              className='altrack-truck-info'>

            <Form.Item label="Title">
                {getFieldDecorator('title', {
                    initialValue: title,
                    rules: [{
                        required: true,
                        message: 'Please input valid Route title',
                        pattern: new RegExp("[a-zA-Zа-яА-Я0-9]+")
                    }],
                })(
                    <Input placeholder='Route title' />
                )}
            </Form.Item>

            <Form.Item block className='p-0 m-0'>
                <Button.Group>
                    <Button htmlType='button'
                            onClick={resetForm}>
                        <FontAwesomeIcon icon={faBan} style={{color: 'red'}} />
                    </Button>

                    <Button htmlType='submit'>
                        <FontAwesomeIcon icon={faSave} style={{color: '#3e759b'}}/>
                    </Button>
                 </Button.Group>
            </Form.Item>
        </Form>
    );
};

const RouteControlForm = Form.create({ name: 'routeForm' })(ControlForm);
export default RouteControlForm;