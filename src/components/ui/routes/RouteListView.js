import React from "react";
import {Link} from "react-router-dom";
import {Button, Col, Form, Input, List, Row} from "antd";

import {FontAwesomeIcon} from '@fortawesome/react-fontawesome'
import {faMapMarkerAlt, faInfo, faPlus} from '@fortawesome/free-solid-svg-icons'
import {faEdit} from '@fortawesome/free-regular-svg-icons'
import {fromWayPointToAddress} from "../../../utils/wayPoints";


export default class RouteListView extends React.Component {
    constructor(props) {
        super(props);

        this.search = this.search.bind(this);
    }

    async componentDidMount() {
        // TODO after saving route, it sometimes fetches route list with non updated route, especially When route line is large
        await this.fetchRoutes();
    }

    async fetchRoutes(params = {}) {
        await this.props.fetchRoutes(params);
    }

    async search(title) {
        await this.fetchRoutes({title});
    }

    render() {
        const {isLoading, routes} = this.props;

        return (
            <Row>
                <Col span={6}>
                    <div className="card border-bottom" style={{border: 'none'}}>
                        <div className="card-body p-0 m-2">
                            <h5 className="card-title altrack-card-header-text">New route</h5>
                            <Link to='/routes/create/'>
                                <Button htmlType='button'
                                        type='primary'>
                                    <FontAwesomeIcon icon={faPlus}/>
                                    <span className='ml-1'>ADD</span>
                                </Button>
                            </Link>
                        </div>
                    </div>
                    <br/>

                    <RouteSearchForm onSearch={this.search}/>
                    <br/>

                    <div className="card border-bottom altrack-truck-info" style={{border: 'none'}}>
                        <div className="card-body p-0 m-2">
                            <h5 className="card-title altrack-card-header-text">Notes</h5>
                            <div>
                                <FontAwesomeIcon icon={faMapMarkerAlt}
                                                 style={{color: '#F9BB5E'}}/>
                                <span className='ml-1'>- Start address</span>
                            </div>
                            <div className='mt-2'>
                                <FontAwesomeIcon icon={faMapMarkerAlt}
                                                 style={{color: '#5F8EEA'}}/>
                                <span className='ml-1'>- Stop address</span>
                            </div>
                        </div>
                    </div>
                </Col>

                <Col span={17} offset={1}>

                    <List grid={{gutter: 20, column: 2}}
                          loading={isLoading}
                          dataSource={routes}
                          pagination={{pageSize: 6}}
                          renderItem={route => (
                              <List.Item>
                                  <RouteListItem route={route}/>
                              </List.Item>
                          )}
                    />
                </Col>
            </Row>
        );
    }


}


class RouteSearchFormView extends React.Component {
    constructor(props) {
        super(props);

        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleSubmit(e) {
        e.preventDefault();

        this.props.form.validateFields((err, values) => {
            if (!err)
                this.props.onSearch(values.title);
        });
    }

    render() {
        const {getFieldDecorator} = this.props.form;

        return (
            <div className="card border-bottom altrack-truck-info" style={{border: 'none'}}>
                <div className="card-body p-0 m-2">
                    <h5 className="card-title altrack-card-header-text">Search</h5>
                    <Form className="login-form">
                        <Form.Item label='Route title'>
                            {getFieldDecorator('title', {
                                rules: [{required: false, message: 'Please input route title!'}],
                            })(
                                <Input placeholder="Route title" onKeyUp={this.handleSubmit}/>
                            )}
                        </Form.Item>
                    </Form>
                </div>
            </div>
        );
    }
}


export const RouteSearchForm = Form.create({name: 'searchRoute'})(RouteSearchFormView);


class RouteListItem extends React.Component {
    render() {
        const {route} = this.props;
        return (
            <div className="card border-bottom altrack-truck-info mt-2"
                 style={{border: 'none', height: '12em'}}>

                <div className="card-body p-0 m-2">
                    <h5 className="card-title font-weight-bold">
                        <Col span={21}>
                            {route.title}
                        </Col>
                        <Col span={1}>
                            <Link to={`/routes/${route.id}/`}
                                  style={{color: 'black'}}>

                                <FontAwesomeIcon icon={faInfo}/>
                            </Link>
                        </Col>
                        <Col span={1}>
                            <Link to={`/routes/${route.id}/edit`}
                                  style={{color: 'black'}}>
                                <FontAwesomeIcon icon={faEdit} offset={1}/>
                            </Link>
                        </Col>
                    </h5>

                    <br/>
                    <br/>

                    <Row>
                        <Col span={1}>
                            <FontAwesomeIcon icon={faMapMarkerAlt}
                                             style={{color: '#F9BB5E'}}/>
                        </Col>
                        <Col span={23}>
                            {fromWayPointToAddress(route.startAddress)}
                        </Col>
                    </Row>
                    <br/>
                    <Row>
                        <Col span={1}>
                            <FontAwesomeIcon icon={faMapMarkerAlt}
                                             style={{color: '#5F8EEA'}}/>
                        </Col>
                        <Col span={23}>
                            {fromWayPointToAddress(route.stopAddress)}
                        </Col>
                    </Row>
                </div>
            </div>
        );
    }
}