import React from "react";
import {Map, TileLayer} from "react-leaflet";


const MapView = (props) => (
    <Map
        style={props.style}
        center={props.center}
        onClick={props.onClick}
        zoom={props.zoom}>

        <TileLayer
                url="https://api.tiles.mapbox.com/v4/mapbox.streets/{z}/{x}/{y}.png?access_token=pk.eyJ1IjoiYnViYXRlYW0iLCJhIjoiY2pyYzNsNTJzMTY4ODN6bDhlMzRpOHRlayJ9.rt-HxxiLNbEbAy1Vzg1foQ"
                attribution="<attribution>" />

        {props.children}
    </Map>
);

MapView.defaultProps = {
    style: {height: "75vh"},
    center: [49.377710, 31.470635],
    zoom: 6,
    onClick: f=>f
};

export default MapView;