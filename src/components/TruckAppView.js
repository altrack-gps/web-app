import React, {Component} from 'react';
import { BrowserRouter as Router } from 'react-router-dom';

import 'antd/dist/antd.css';

import BaseRouter from "../routes";
import CustomLayout from "./Layout";


export default class TruckAppView extends Component {
    componentDidMount() {
        this.props.onTryAutoSignup();
    }

    render() {
        return (
            <div>
                <Router>
                    <CustomLayout {...this.props}>
                        <BaseRouter {...this.props} />
                    </CustomLayout>
                </Router>
            </div>
        );
    }
}
