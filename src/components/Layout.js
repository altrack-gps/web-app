import React from "react";
import {Layout, Menu} from "antd";
import {Link} from "react-router-dom";


const { Header, Content, Footer } = Layout;


const CustomLayout = (props) => (
    <Layout className="layout">
        <Header>
            <div className="logo"/>
            {
                props.isAuthenticated ?
                    <AuthenticatedMenu activeMenuIndex={props.menuIndex}
                                       onLogout={props.onLogout}
                                       onMenuSelect={props.onMenuSelect}/> :
                    <NotAuthenticatedMenu/>
            }
        </Header>
        <Content style={{padding: '50px 50px 0 50px'}}>
            <div style={{background: '#fff', padding: 24, minHeight: 780}}>
                {props.children}
            </div>
        </Content>
        <Footer style={{textAlign: 'center'}}>
            ALTrack ©2019 Created by p1ncher
        </Footer>
    </Layout>
);


const AuthenticatedMenu = ({activeMenuIndex, onLogout, onMenuSelect}) => (
    <Menu
        theme="dark"
        mode="horizontal"
        onClick={e => onMenuSelect(e.key)}
        selectedKeys={[activeMenuIndex.toString()]}
        className='altrack-card-subtitle-link'
        defaultSelectedKeys={['3']}
        style={{lineHeight: '64px'}}>

        <Menu.Item key="3">
            <Link to='/'>Home</Link>
        </Menu.Item>

        <Menu.Item key="4">
            <Link to='/trucks'>Trucks</Link>
        </Menu.Item>

        <Menu.Item key="6">
            <Link to='/truck-reports/'>Truck reports</Link>
        </Menu.Item>

        <Menu.Item key="5">
            <Link to='/routes'>Routes</Link>
        </Menu.Item>
        <Menu.Item key="1" onClick={onLogout} style={{float: 'right'}}>
            <Link to="/logout">Logout</Link>
        </Menu.Item>
    </Menu>
);


const NotAuthenticatedMenu = () => (
    <Menu
        theme="dark"
        mode="horizontal"
        className='altrack-card-subtitle-link'
        defaultSelectedKeys={['1']}
        style={{lineHeight: '64px', float: 'right'}}>

            <Menu.Item key="1">
                <Link to="/login">Login</Link>
            </Menu.Item>
    </Menu>
);



export default CustomLayout;