import {connect} from 'react-redux'
import {withRouter} from "react-router-dom";

import TruckAppView from "./TruckAppView";

import LoginView from "./ui/LoginView";
import TruckListView from "./ui/trucks/TruckListView";
import EditRouteView from "./ui/routes/EditRouteView";
import RouteListView from "./ui/routes/RouteListView";
import * as Listener from "./ui/trucks/TruckEventListener";
import TruckEventView from "./ui/trucks/TruckEventView";
import TruckDetailView from "./ui/trucks/TruckDetailView";
import {PointListView} from "./ui/routes/PointListView";
import CreateTruckForm from "./ui/trucks/CreateTruckForm";
import CreateRouteView from "./ui/routes/CreateRouteView";
import RouteDetailView from "./ui/routes/RouteDetailView";
import TruckGPSStateView from "./ui/trucks/TruckGPSState";
import TruckEventListView from "./ui/trucks/TruckEventsListView";

import {
    addPoint,
    saveRoute,
    fetchRoute,
    resetRoute,
    updateRoute,
    createRoute,
    removeRoute,
    removePoint,
    updatePoint,
    onFetchRoutes,
    updateWayPointAddress,
} from "../store/actions/routes";

import {
    fetchTrucks,
    createTruck,
    removeTruck,
    setTruckRoute,
    onFetchTruck, saveGpsState
} from "../store/actions/trucks";

import {saveEvent, fetchLastEvent} from "../store/actions/events";
import {authCheckState, authLogin, logout} from "../store/actions/auth";
import {updateMenuIndex} from "../store/actions/base";


export const TruckApp = withRouter(
    connect(
        state => ({
            isAuthenticated: state.auth.token !== null,
            menuIndex: state.pageState.menuIndex
        }),
        dispatch => ({
            onTryAutoSignup() {
                dispatch(authCheckState());
            },
            onLogout() {
                dispatch(logout());
                window.location.replace('/login');
            },
            onMenuSelect(menuIndex) {
                dispatch(updateMenuIndex(menuIndex))
            }
        })
    )(TruckAppView)
);


export const TruckList = connect(
    ({trucks}) => ({trucks}),
    dispatch => ({
        async onFetchTrucks(params={}) {
            dispatch(await fetchTrucks(params));
        },
    })
)(TruckListView);


export const CreateTruck = connect(
    null,
    dispatch => ({
        async onCreateTruck(imei, plateNumber) {
            dispatch(await createTruck(imei, plateNumber))
        }
    })
)(CreateTruckForm);


export const TruckDetail = connect(
    (state, props) => ({
        id: Number(props.match.params.id),
        truck: state.truck,
        route: state.route,
        gpsState: state.lastGPSState
    }),
    dispatch => ({
        async fetchTruck(id) {
            dispatch(await onFetchTruck(id));
        },
        async fetchRoute(id) {
            dispatch(await fetchRoute(id));
        },
        async setRoute(truckID, routeID) {
            dispatch(await setTruckRoute(truckID, routeID));
        }
    })
)(TruckDetailView);


export const RouteList = connect(
    (state) => ({
        routes: state.routes,
        isLoading: state.pageState.routes.isFetching
    }),
    dispatch => ({
        async removeRoute(id) {
            dispatch(await removeRoute(id));
        },
        async fetchRoutes(params) {
            dispatch(await onFetchRoutes(params));
        }
    })
)(RouteListView);


const routeMapDispatchToProps = dispatch => ({
    async addWayPoint(wayPoint) {
        dispatch(await addPoint(wayPoint))
    },
    async updateWayPoint(wayPoint) {
        dispatch(await updatePoint(wayPoint))
    },
    updateRoute(route) {
        dispatch(updateRoute(route));
    },
    resetRoute() {
        dispatch(resetRoute());
    },
});


export const CreateRoute = connect(
    ({route}) => ({route}),
    dispatch => ({
        async saveRoute(route) {
            dispatch(await createRoute(route));
        },
        ...routeMapDispatchToProps(dispatch)
    })
)(CreateRouteView);


export const EditRoute = connect(
    (state, props) => ({
        route: state.route,
        id: props.match.params.id
    }),
    dispatch => ({
        async fetchRoute(id) {
            dispatch(await fetchRoute(id));
        },
        async saveRoute(id, route) {
            dispatch(await saveRoute(id, route))
        },
        ...routeMapDispatchToProps(dispatch)
    })
)(EditRouteView);


export const RouteDetail = connect(
    (state, props) => ({
        route: state.route,
        id: props.match.params.id
    }),
    dispatch => ({
        async fetchRoute(id) {
            dispatch(await fetchRoute(id));
        }
    })
)(RouteDetailView);


export const PointList = connect(
    null,
    dispatch => ({
        removeWayPoint(number) {
            dispatch(removePoint(number));
        },
        async updateWayPoint(wayPoint) {
            dispatch(await updatePoint(wayPoint));
        },
        updateWayPointAddress(wayPoint) {
            dispatch(updateWayPointAddress(wayPoint));
        }
    })
)(PointListView);


export const TruckEvent = connect(
    (state, props) => ({
        event: state.lastEvent,
        truckID: (props.truckID) ? props.truckID : props.match.params.truckID
    }),
    dispatch => ({
        async fetchLastEvent(truckID) {
            dispatch(await fetchLastEvent(truckID));
        }
    })
)(TruckEventView);


export const TruckEventListener = connect(
    null,
    dispatch => ({
        saveEvent(e) {
            dispatch(saveEvent(e));
        }
    })
)(Listener.default);


export const TruckEventList = connect(
    (state, props) => ({
        truckID: props.match.params.id
    }),
    null
)(TruckEventListView);


export const TruckGPSState = connect(
    null,
    dispatch => ({
        saveGpsState(state) {
            dispatch(saveGpsState(state));
        }
    })
)(TruckGPSStateView);


export const Login = connect(
    null,
    dispatch => ({
        async onAuth(login, password) {
            dispatch(await authLogin(login, password));
        }
    })
)(LoginView);