import React from 'react';
import { Route, Switch } from 'react-router-dom';

import Home from "./components/ui/HomePage";
import {
    Login,
    EditRoute,
    RouteList,
    TruckList,
    CreateRoute,
    CreateTruck,
    TruckDetail,
    TruckEventList,
    RouteDetail,
} from "./components/containers";
import TruckHistoryView from "./components/ui/trucks/TruckHistoryView";
import TruckReportListView from "./components/ui/trucks/TruckReportListView";
import TruckStatsView from "./components/ui/trucks/TruckStatsView";


const BaseRouter = ({isAuthenticated}) => (
    <div>
        {(isAuthenticated) ?
            <AuthenticatedRoutes/> :
            <NotAuthenticatedRoutes/>}
    </div>
);

export default BaseRouter;


const AuthenticatedRoutes = () => (
    <Switch>
        <Route exact path="/" component={Home} />
        <Route path="/trucks/create" component={CreateTruck} />
        <Route path="/trucks/:id/history" component={TruckHistoryView} />
        <Route path="/trucks/:id" component={TruckDetail} />
        <Route path="/trucks/" component={TruckList} />

        <Route path="/truck-stats/:id" component={TruckStatsView} />
        <Route path="/truck-events/:id" component={TruckEventList} />
        <Route path="/truck-reports/" component={TruckReportListView} />

        <Route path="/routes/create/" component={CreateRoute} />
        <Route path="/routes/:id/edit/" component={EditRoute} />
        <Route path="/routes/:id/" component={RouteDetail} />
        <Route path="/routes/" component={RouteList} />

        <Route path="/login" component={Login} />
    </Switch>
);


const NotAuthenticatedRoutes = () => (
    <Switch>
        <Route path="/login" component={Login} />
    </Switch>
);