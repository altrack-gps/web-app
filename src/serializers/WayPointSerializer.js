export default class WayPointSerializer {
    static serialize(wayPoint) {
        return {
            number: wayPoint.number,
            position: {
                lat: wayPoint.lat,
                lng: wayPoint.lng
            },
            address: {
                houseNumber: wayPoint.houseNumber,
                road: wayPoint.road,
                locality: wayPoint.locality,
                localityType: wayPoint.localityType,
                state: wayPoint.state,
                country: wayPoint.country
            }
        };
    }

    static deserialize(wayPoint) {
        return {
            id: wayPoint.id,
            number: wayPoint.number,
            isControlPoint: true,
            ...wayPoint.address,
            ...wayPoint.position,
        }
    }
}