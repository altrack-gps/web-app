import WayPointService from "../services/WayPointService";
import WayPointSerializer from "./WayPointSerializer";


export default class RouteSerializer {
    static serialize(route) {
        const wayPoints = [...route.wayPoints];
        const controlPoints = WayPointService.excludeControlPoints(wayPoints);

        return {
            ...route,
            wayPoints: controlPoints.map(
                wp => WayPointSerializer.serialize(wp)
            )
        }
    }

    static deserialize(route) {
        const wayPoints = [...route.wayPoints];
        return {
            ...route,
            wayPoints: wayPoints.map(
                wp => WayPointSerializer.deserialize(wp)
            )
        }
    }
}