import {del, get, post, put} from "./base";


export const fetchEvents = (truckID, params={}) => get(`/truck-events/${truckID}`, {params});
export const fetchLastEvent = truckID => get(`/truck-events/${truckID}/last`);


export const saveRoute = (id, route) => put(`/routes/${id}/`, route);
export const removeRoute = id => del(`/routes/${id}/`);
export const createRoute = route => post('/routes/', route);
export const fetchRoute = id => get(`/routes/${id}/`);
export const fetchOnlyRouteTitles = (params={}) => get('/routes/titles', {params});
export const fetchRoutesWithAddresses = (params={}) => get('/routes/with_addresses/', {params});


export const fetchTrucks = params => get('/trucks/', {params});
export const fetchTruck = id => get(`/trucks/${id}`);
export const fetchTruckTitles = () => get('/trucks/titles');
export const removeTruck = id => del(`/trucks/${id}`);
export const createTruck = truck => post('/trucks/', truck);
export const setRoute = (truckID, routeID) => put(`/trucks/${truckID}/set_route/`, {routeID: routeID});
export const fetchTruckHistory = (truckID, params={}, filters={}) => get(`/truck-history/${truckID}`, {params}, filters);


export const fetchReports = (params={}) => get('/truck-reports/', {params});
export const fetchTruckStats = (id, params={}) => get(`/truck-reports/finished-routes/${id}`, {params});