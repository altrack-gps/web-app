import axios from 'axios';
import {showError} from "../utils/messages";


const apiURL = 'http://127.0.0.1:8000/api/';
const instance = axios.create({
    baseURL: apiURL,
    headers: {
        "Content-Type": "application/json",
    }
});


export const makeQuery = caller => {
    return async function f() {
        try {
            const paramsKey = Math.max(...Object.keys(arguments));
            arguments[paramsKey].headers = {
                Authorization: `Token ${localStorage.getItem('token')}`
            };
            const response = await caller.apply(null, arguments);
            return response.data;

        } catch (e) {
            if (Object.keys(e).indexOf('response') > 0) {
                const {response} = e;

                if (response.status === 401)
                    window.location.replace('/login');
                else if (response.status >= 400)
                    showError('Something went wrong. Try again');
            }
            else {
                showError('Unrecognized error. Please connect with developer');
                console.error(e);
            }
        }
    }
};


export const get = (url, params={}) => makeQuery(instance.get)(url, params);
export const post = (url, params) => makeQuery(instance.post)(url, params);
export const put = (url, params={}) => makeQuery(instance.put)(url, params);
export const del = (url, params={}) => makeQuery(instance.delete)(url, params);
