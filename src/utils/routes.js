import * as L from "leaflet";
import 'leaflet.awesome-markers';


export const allAddressesFilled = wayPoints => {
    for (let wp of wayPoints) {
        if (wp.isControlPoint && (!wp.houseNumber || !wp.road || !wp.locality || !wp.localityType)) {
            return false;
        }
    }
    return true;
};


export const stringifyPoints = (points) => {
    let coordString = points.reduce(
        (str, point) => str + `${point.lng},${point.lat};`, ''
    );
    coordString = coordString.slice(0, -1);
    return coordString;
};


export const getPointIcon = (point, points) => {
    const start = 0;
    const end = points.length - 1;

    if (point.number === start)
        return StartPointIcon;

    else if (point.number === end)
        return EndPointIcon;

    else if (point.isControlPoint)
        return ControlPointIcon;

    else
        return DefaultIcon;
};

const getIcon = color => L.AwesomeMarkers.icon({
    markerColor: color,
    tooltipAnchor: [16, -28],
});

const StartPointIcon = getIcon('green');
const EndPointIcon = getIcon('darkred');
const ControlPointIcon = getIcon('orange');
const DefaultIcon = getIcon('blue');
