import * as moment from 'moment';


export const fromSecondsToTime = seconds => {
    const duration = moment.duration(Math.floor(seconds), 'seconds');
    return `${duration.days()} ${duration.hours()}:${duration.minutes()}:${duration.seconds()}`;
};


const _formatDateTime = (dt_str, format, utc=false) => {
    const func = utc ? moment.utc : moment;
    return (dt_str) ? func(dt_str).format(format) : '';
};


export const formatLocalDateTime = dt_str =>
    _formatDateTime(dt_str, 'YYYY-MM-DD HH:mm:ss');


export const formatUTCDateTime = dt_str =>
    _formatDateTime(dt_str, 'YYYY-MM-DD HH:mm:ss', true);


export const formatDate = date_str =>
    _formatDateTime(date_str, 'YYYY-MM-DD');