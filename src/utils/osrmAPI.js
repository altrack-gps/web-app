import axios from 'axios'
import {fromSecondsToTime} from "./time";
import * as routes from "./routes";
import {showError} from "./messages";


class Service {
    static TABLE = 'table';
    static DRIVING = 'driving';
}


const stringifyPointsdd = (points) => {
    let coordString = points.reduce(
        (str, point) => str + `${point[1]},${point[0]};`, ''
    );
    coordString = coordString.slice(0, -1);
    return coordString;
};


const makeOSRMQuery = async (service, points, params={}) => {
    let strFunc = (Array.isArray(points[0])) ? stringifyPointsdd : routes.stringifyPoints;
    const coordStr = strFunc(points);
    const url = `http://localhost:5000/route/v1/${service}/${coordStr}`;

    try {
        const response = await axios.get(
            url, {
                params: {
                    geometries: 'geojson',
                    continue_straight: false,
                    overview: false,
                    ...params
                }
            }
        );
        return excludeRoute(response.data);
    }
    catch (e) {
        const excHasResponse = Object.keys(e).indexOf('response') > 0;
        if (excHasResponse) {
            const {response} = e;
            if (response.data.code === 'TooBig')
                showError('Requested route is too big. Try make shorter date range.');
        }
        else
            showError('Error has happened during building of route. Try again or later!')
    }
};


export const computeDuration = async (points, params={}) =>
    await makeOSRMQuery(Service.TABLE, points, params);


export const buildRoute = async (points, params={}) =>
    await makeOSRMQuery(Service.DRIVING, points, params);


const excludeRoute = response => {
    const route = response.routes[0];
    const duration = fromSecondsToTime(route.duration);
    const distance = Number((route.distance / 1000).toFixed(2));

    return {
        ...route,
        distance,
        duration,
    }
};