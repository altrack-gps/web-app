import axios from 'axios';
import {getLocality, LocalityTypes, LocalityTypeValues} from "./localities";


export const getWayPointWithAddress = async coordinates => {
    const address = await fetchWayPointAddress(coordinates);
    return fromAddressToWayPoint(address, coordinates);
};


const fromAddressToWayPoint = (address, coordinates) => ({
    road: (address.road) ? address.road : null,
    state: (address.state) ? address.state : null,
    country: address.country,
    houseNumber: (address.house_number) ? address.house_number : null,
    ...getLocality(address),
    ...coordinates
});


const fetchWayPointAddress = async coordinates => {
    const url = `https://nominatim.openstreetmap.org/reverse?format=jsonv2&lat=${coordinates.lat}&lon=${coordinates.lng}&zoom=18&addressdetails=1`;
    const response = await axios.get(url);

    return response.data.address;
};


export const fromWayPointToAddress = point => {
    return `${displayHouse(point.houseNumber)}, 
            ${displayString(point.road)},
            ${displayLocality(point.locality, point.localityType)}, 
            ${displayString(point.state)}, 
            ${displayString(point.country)}`;
};

const displayHouse = houseNumber => (houseNumber) ? `буд. ${houseNumber}` : '';

const displayString = str => (str) ? str : '';

const displayLocality = (locality, localityType) => {
    return `${(localityType === LocalityTypes.CITY) ? LocalityTypeValues.CITY : LocalityTypeValues.VILLAGE} 
        ${displayString(locality)}`
};