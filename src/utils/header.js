const MENU_KEYS = {
    '/trucks': 4,
    '/home': 3,
    '/truck-reports/': 6,
    '/routes': 5,

};


export const DEFAULT_MENU_INDEX = 3;


export const getCurrentMenuKey = () => {
    const currentKey = MENU_KEYS[window.location.pathname];
    return currentKey ? currentKey : DEFAULT_MENU_INDEX
};
