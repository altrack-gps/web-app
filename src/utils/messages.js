import {notification, message} from "antd";


export const showNotification = text => {
    notification['info']({
        message: text,
    });
};


export const showWarning = text => {
    notification['warning']({
        message: text,
    });
};


export const showMessage = (text, seconds=1) => {
    message.success(text, seconds);
};


export const showError = (text, seconds=3) => {
    message.error(text, seconds);
};


export const openNotification = (msg, description, duration=0) => {
    const args = {
        message: msg,
        description: description,
        duration: duration,
    };
    notification.open(args);
};
