export const LocalityTypeValues = {
    CITY: 'м.',
    VILLAGE: 'с.'
};

export class LocalityTypes {
    static CITY = 'CITY';
    static VILLAGE = 'VILLAGE';
}


export const getLocality = address => {
    if (address.city !== undefined)
        return {
            locality: address.city,
            localityType: LocalityTypes.CITY
        };
    else if (address.town !== undefined)
        return {
            locality: address.town,
            localityType: LocalityTypes.CITY
        };
    else if (address.village !== undefined)
        return {
            locality: address.village,
            localityType: LocalityTypes.VILLAGE
        };
    else {
        return {
            locality: null,
            localityType: null
        }
    }
};