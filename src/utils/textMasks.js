import msk from "msk";


export const imeiMask = (imei, mask='9999 9999 9999 9999') =>
    msk(imei, mask);


export const plateNumberMask = (plateNumber, mask='99 9999 99') =>
    msk(plateNumber, mask);

