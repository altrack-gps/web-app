import thunk from 'redux-thunk';
import {createStore, combineReducers, applyMiddleware, compose} from 'redux'

import auth from "./reducers/auth";
import pageState from "./reducers/pageState";
import {lastGPSState, truck, trucks} from "./reducers/trucks";
import {route, routes} from "./reducers/routes";
import {lastEvent} from "./reducers/events";


const composeEnhances = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const reducers = combineReducers({
    truck,
    trucks,
    auth,
    routes,
    pageState,
    route,
    lastEvent,
    lastGPSState
});

const storeFactory = (initialState={}) =>
    composeEnhances(
        applyMiddleware(thunk)
    )(createStore)(
        reducers,
        initialState
    );

export default storeFactory;