import axios from 'axios'
import * as actionTypes from './actionTypes'
import {showError} from "../../utils/messages";


export const authStart = () => {
    return {
        type: actionTypes.AUTH_START
    }
};


export const authSuccess = token => {
    return {
        type: actionTypes.AUTH_SUCCESS,
        token: token
    }
};


export const authFail = error => {
    showError('Error. Enter correct login or password!');
    return {
        type: actionTypes.AUTH_FAIL,
        error: error
    }
};


let checkAuthTimeout = expirationTime => {
    return dispatch => {
        setTimeout(() => {
            dispatch(logout());
        }, expirationTime * 1000);
    }
};


export const logout = () => {
    localStorage.removeItem('token');
    localStorage.removeItem('expirationDate');

    return {
        type: actionTypes.AUTH_LOGOUT
    };
};


export const authLogin = (username, password) => {
    return async dispatch => {
        dispatch(authStart());

        try {
            let response = await axios.post(
                'http://localhost:8000/rest-auth/login/', {
                    username, password
                }
            );
            const token = response.data.key;
            const expirationTime = new Date(new Date().getTime() + 3600 * 1000);

            localStorage.setItem('token', token);
            localStorage.setItem('expirationDate', expirationTime);

            dispatch(authSuccess(token));
            dispatch(checkAuthTimeout(3600));
            window.location.replace('/');
        } catch (e) {
            dispatch(authFail(e));
            // window.location.replace('/login');
        }
    }
};


export const authCheckState = () => {
    return dispatch => {
        const token = localStorage.getItem('token');

        if (token === undefined)
            dispatch(logout());
        else {
            const expirationDate = new Date(localStorage.getItem('expirationDate'));
            if (expirationDate < new Date())
                dispatch(logout());
            else {
                dispatch(authSuccess(token));
                dispatch(checkAuthTimeout((expirationDate.getTime() - new Date().getTime()) / 1000));
            }
        }
    }
};