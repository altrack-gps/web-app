import * as osrm from "../../utils/osrmAPI";
import * as api from "../../altrackAPI/api";
import {showError, showMessage} from "../../utils/messages";
import * as actionTypes from "../actions/actionTypes"
import {getWayPointWithAddress} from "../../utils/wayPoints";
import WayPointService from "../../services/WayPointService";
import RouteSerializer from "../../serializers/RouteSerializer";
import {resetState} from "../reducers/routes";


export const buildRoute = () =>
    async (dispatch, getState) => {
        let route = {
            duration: 0,
            distance: 0,
            line: []
        };
        const {wayPoints} = getState().route;

        if (wayPoints.length > 1) {
            const builtRoute = await osrm.buildRoute(wayPoints, {overview: 'full'});
            console.log(builtRoute);
            route.duration = builtRoute.duration;
            route.distance = builtRoute.distance;
            route.line = builtRoute.geometry.coordinates.map(
                item => [item[1], item[0]]
            );
        }
        dispatch(updateRoute(route));
    };


export const createRoute = async route => {
    const newRoute = await api.createRoute(
        RouteSerializer.serialize(route)
    );
    showMessage('Route has been created');

    return {
        type: actionTypes.CREATE_ROUTE,
        payload: {
            ...RouteSerializer.deserialize(newRoute)
        }
    };
};


export const saveRoute = async (id, route) => {
    const updatedRoute = await api.saveRoute(
        id,
        RouteSerializer.serialize(route)
    );
    showMessage('Route has been saved');

    return {
        type: actionTypes.EDIT_ROUTE,
        payload: {
            ...RouteSerializer.deserialize(updatedRoute)
        }
    }
};


export const resetRoute = () => ({
    type: actionTypes.RESET_ROUTE
});


export const removeRoute = async id => {
    try {
        await api.removeRoute(id);
        return {
            type: actionTypes.REMOVE_ROUTE,
            id: id
        }
    } catch (e) {
        showError('Route has not been deleted!');
    }
};


export const fetchingStarted = () => ({
    type: actionTypes.FETCH_ROUTES_START
});


export const fetchingEnded = () => ({
    type: actionTypes.FETCH_ROUTES_END
});


export const onFetchRoutes = params =>
    async dispatch => {
        dispatch(resetState());
        dispatch(fetchingStarted());
        const routes = dispatch(await fetchRoutes(params));
        dispatch(fetchingEnded());

        return routes;
    };


const fetchRoutes = async params => {
    const routes = await api.fetchRoutesWithAddresses(params);
    return {
        type: actionTypes.FETCH_ROUTES,
        routes: routes
    };
};


export const fetchRoute = async id => {
    let route = await api.fetchRoute(id);
    return {
        type: actionTypes.FETCH_ROUTE,
        payload: {
           ...RouteSerializer.deserialize(route)
        }
    }
};


export const addPoint = coordinates =>
    async (dispatch, getState) => {
        dispatch(await addWayPoint(coordinates));
        dispatch(await buildRoute());

        const wayPointsLength = getState().route.wayPoints.length;
        WayPointService.showNotification(wayPointsLength);
    };


const addWayPoint = async coordinates => {
    const wayPoint = await getWayPointWithAddress(coordinates);
    return {
        type: actionTypes.CREATE_WAY_POINT,
        payload: {...wayPoint}
    }
};


const updateWayPoint = async wayPoint => {
    const updatedWayPoint = await getWayPointWithAddress({
        lat: wayPoint.lat,
        lng: wayPoint.lng
    });
    return {
        type: actionTypes.UPDATE_WAY_POINT,
        payload: {
            ...wayPoint,
            ...updatedWayPoint
        }
    };
};


export const updateWayPointAddress = wayPoint => {
    return {
        type: actionTypes.UPDATE_WAY_POINT_ADDRESS,
        payload: {...wayPoint}
    };
};


export const updatePoint = wayPoint =>
    async dispatch => {
        dispatch(await updateWayPoint(wayPoint));
        dispatch(await buildRoute());
    };


const removeWayPoint = number => ({
    type: actionTypes.REMOVE_WAY_POINT,
    number: number
});


export const removePoint = number =>
    async (dispatch, getState) => {
        dispatch(removeWayPoint(number));
        dispatch(await buildRoute());

        const wayPointsLength = getState().route.wayPoints.length;
        WayPointService.showNotification(wayPointsLength);
    };


export const updateRoute = route => {
    return {
        type: actionTypes.UPDATE_ROUTE,
        payload: {...route},
    }
};
