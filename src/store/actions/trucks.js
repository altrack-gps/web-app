import * as api from '../../altrackAPI/api';
import * as actionTypes from './actionTypes'
import {showMessage} from "../../utils/messages";
import {fetchRoute, resetRoute} from "./routes";
import {resetState} from "../reducers/routes";


export const createTruck = async (imei, plateNumber) => {
    const truck = await api.createTruck({
        imei: imei,
        plateNumber: plateNumber
    });

    return {
        type: actionTypes.CREATE_TRUCK,
        payload: {...truck}
    }
};


export const setTruckRoute = (id, routeID) =>
    async dispatch => {
        dispatch(await setRoute(id, routeID));
        dispatch(resetRoute());
        if (routeID)
            dispatch(await fetchRoute(routeID));
    };

const setRoute = async (id, routeID) => {
    await api.setRoute(id, routeID);
    showMessage('Truck route has been updated');
    return {
        type: actionTypes.UPDATE_TRUCK_ROUTE,
        payload: {
            id: id,
            routeID: routeID
        }
    }
};


export const removeTruck = async id => {
    await api.removeTruck(id);
    return {
        type: actionTypes.REMOVE_TRUCK,
        id: id
    }
};


export const fetchTrucks = async params => {
    const trucks = await api.fetchTrucks(params);
    return {
        type: actionTypes.FETCH_TRUCKS,
        payload: trucks
    };
};


export const onFetchTruck = id =>
    async dispatch => {
        dispatch(resetState());
        const truck = dispatch(await fetchTruck(id));

        if (truck.payload.routeID)
            dispatch(await fetchRoute(truck.payload.routeID));
    };


const fetchTruck = async id => {
    const truck = await api.fetchTruck(id);
    return {
        type: actionTypes.FETCH_TRUCK,
        payload: truck
    }
};


export const saveGpsState = state => ({
    type: actionTypes.SAVE_GPS_STATE,
    payload: state
});