import * as actionTypes from "./actionTypes";
import {getCurrentMenuKey} from "../../utils/header";


export const updateMenuIndex = () => ({
    type: actionTypes.UPDATE_MENU_INDEX,
    menuIndex: getCurrentMenuKey()
});
