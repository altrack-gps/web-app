import * as api from "../../altrackAPI/api";
import * as actionTypes from "./actionTypes";


export const saveEvent = event => ({
    type: actionTypes.SAVE_EVENT,
    payload: event
});


export const fetchLastEvent = async truckID => {
    const event = await api.fetchLastEvent(truckID);
    return {
        type: actionTypes.FETCH_EVENT,
        payload: event
    }
};