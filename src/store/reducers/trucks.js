import * as actionTypes from "../actions/actionTypes"


export const truck = (state={}, action) => {
    switch (action.type) {
        case actionTypes.FETCH_TRUCK:
        case actionTypes.CREATE_TRUCK:
            return {
                ...action.payload
            };
        case actionTypes.UPDATE_TRUCK_ROUTE:
            return {
                ...state,
                routeID: action.payload.routeID
            };
        case actionTypes.RESET_TRUCK:
            return {};
        default:
            return state;
    }
};


export const trucks = (state=[], action) => {
    switch (action.type) {
        case actionTypes.REMOVE_TRUCK:
            return state.filter(
                truck => truck.id !== action.id
            );
        case actionTypes.FETCH_TRUCKS:
            return [...action.payload];
        default:
            return state;
    }
};


export const lastGPSState = (state={}, action) => {
    switch (action.type) {
        case actionTypes.SAVE_GPS_STATE:
            return {
                ...action.payload
            };
        case actionTypes.RESET_GPS_STATE:
            return {};
        default:
            return state;
    }
};