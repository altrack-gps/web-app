import * as actionTypes from "../actions/actionTypes";


export const lastEvent = (state={}, action) => {
    switch (action.type) {
        case actionTypes.FETCH_EVENT:
        case actionTypes.SAVE_EVENT:
            return {
                ...action.payload
            };
        case actionTypes.RESET_EVENT:
            return {};
        default:
            return state;
    }
};