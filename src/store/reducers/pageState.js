import {FETCH_ROUTES_END, FETCH_ROUTES_START, UPDATE_MENU_INDEX} from "../actions/actionTypes";
import {getCurrentMenuKey} from "../../utils/header";


const DEFAULT_STATE = {
    routes: {
        isFetching: false
    },
    menuIndex: getCurrentMenuKey()
};


const pageState = (state=DEFAULT_STATE, action) => {
    switch (action.type) {
        case FETCH_ROUTES_START:
            return {
                ...state,
                routes: {
                    isFetching: true
                }
            };

        case FETCH_ROUTES_END:
            return {
                ...state,
                routes: {
                    isFetching: false
                }
            };

        case UPDATE_MENU_INDEX:
            return {
                ...state,
                menuIndex: action.menuIndex
            };

        default:
            return state;
    }
};


export default pageState;