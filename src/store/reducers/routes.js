import {updateObject} from "../utils";
import * as actionTypes from '../actions/actionTypes';

import WayPointService from "../../services/WayPointService";
import {resetRoute} from "../actions/routes";


const initNewRoute = {
    title: '',
    line: [],
    duration: 0,
    distance: 0,
    wayPoints: []
};


export const route = (state=initNewRoute, action) => {
    switch (action.type) {
        case actionTypes.FETCH_ROUTE:
        case actionTypes.CREATE_ROUTE:
            return {...action.payload};

        case actionTypes.UPDATE_ROUTE:
            return updateRoute(state, action);

        case actionTypes.RESET_ROUTE:
            return initNewRoute;

        case actionTypes.CREATE_WAY_POINT: {
            const wayPoints = WayPointService.insertInArray(
                state.wayPoints,
                action.payload
            );
            return {
                ...state,
                wayPoints: [...wayPoints]
            }
        }

        case actionTypes.UPDATE_WAY_POINT_ADDRESS:
        case actionTypes.UPDATE_WAY_POINT:
            return updateObject(state, {
                ...state,
                wayPoints: state.wayPoints.map(
                    wp => updateWayPoint(wp, action)
                )
            });

        case actionTypes.REMOVE_WAY_POINT:
            return removeWayPoint(state, action);

        default:
            return state;
    }
};


const updateWayPoint = (state, action) => {
    return (state.number === action.payload.number) ?
        updateObject(state, action.payload) : state;
};


const updateRoute = (state, action) =>
    updateObject(state, {
        ...action.payload
    });


const removeWayPoint = (state, action) => {
    const wayPoints = WayPointService.removeWayPoint(
        state.wayPoints,
        action.number
    );

    return {
        ...state,
        wayPoints: wayPoints
    };
};


export const routes = (state=[], action) => {
    switch (action.type) {
        case actionTypes.REMOVE_ROUTE:
            return state.filter(
                route => route.id !== action.id
            );

        case actionTypes.FETCH_ROUTES:
            return action.routes;
        default:
            return state;
    }
};


const resetTruck = () => ({
    type: actionTypes.RESET_TRUCK,
});

const resetGPSState = () => ({
    type: actionTypes.RESET_GPS_STATE,
});

const resetEvent = () => ({
    type: actionTypes.RESET_EVENT,
});


export const resetState = () => dispatch => {
    dispatch(resetRoute());
    dispatch(resetTruck());
    dispatch(resetGPSState());
    dispatch(resetEvent());
};
