import {showNotification} from "../utils/messages";


export default class WayPointService {
    static findByNumber(points, number) {
        return points.find(wp => wp.number === number);
    }

    static insertInArray(wayPoints, point) {
        let updatedPoints = [...wayPoints];

        if (wayPoints.length <= 1)
            updatedPoints.push(point);
        else
            updatedPoints.splice(updatedPoints.length - 1, 0, point);

        this.updateWayPoints(updatedPoints);
        return updatedPoints;
    }

    static updateWayPoints(wayPoints) {
        this._updateOrder(wayPoints);
        this._updateBoundaries(wayPoints);
    }

    static _updateOrder(wayPoints) {
        wayPoints.forEach((wp, index) => wp.number = index);
    };

    static _updateBoundaries = (wayPoints) => {
        if (wayPoints.length > 0) {
            wayPoints[0].isControlPoint = true;
            wayPoints[wayPoints.length - 1].isControlPoint = true;
        }
    };

    static removeWayPoint(wayPoints, number) {
        let updatedWayPoints = wayPoints.filter(wp => wp.number !== number);
        this.updateWayPoints(updatedWayPoints);

        return updatedWayPoints;
    }

    static excludeControlPoints(wayPoints) {
        let controlPoints = wayPoints.filter(wp => wp.isControlPoint);
        this.updateWayPoints(controlPoints);

        return controlPoints;
    }

    static showNotification(wayPointsLength) {
        let text = '';

        switch (wayPointsLength) {
            case 0:
                text = 'Set start point';
                break;
            case 1:
                text = 'Set end point';
                break;
            default:
                break;
        }

        if (text)
            showNotification(text);
    }
}
